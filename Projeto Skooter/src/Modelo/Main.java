package Modelo;

import Auxiliar.Consts;
import Auxiliar.FaseXmlDomWriter;
import Controler.Tela;
import Fases.ControladorDeFase;
import Fases.ControladorDeColisao;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

    public static void main(String[] args) {
        // Carrega Singletons
        ControladorDeFase controladorDeFase = ControladorDeFase.getSingleton();
        ControladorDeColisao controladorDeColisao = new ControladorDeColisao(Consts.RES, Consts.RES);

        System.out.println("Qual o intervalo de salvamento automatico?");
        Scanner scanner = new Scanner(System.in);
        
        int intervalo;
        intervalo = scanner.nextInt();
                
        //Cria uma thread que roda o jogo
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Tela tTela = new Tela();
                tTela.setVisible(true);
                tTela.createBufferStrategy(2);
                tTela.go();
            }
        });
        
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (ControladorDeFase.getSingleton().getNumeroFase() != 0) {
                    FaseXmlDomWriter.saveToXml(ControladorDeFase.getSingleton().getFaseAtual(), ControladorDeFase.getSingleton().getNumeroFase());
                }
            }
        }, 1000, intervalo * 1000);
    }
}
