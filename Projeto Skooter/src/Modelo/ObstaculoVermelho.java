package Modelo;

import java.io.Serializable;

public class ObstaculoVermelho extends Obstaculo implements Serializable{
    
    public ObstaculoVermelho(String sNomeImagePNG, int id) {
        super(sNomeImagePNG, id);
        this.bTransponivel=false;
        this.bMortal=false;
    }
}
