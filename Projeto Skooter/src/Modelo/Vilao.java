package Modelo;

import Auxiliar.Consts;
import Auxiliar.Posicao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Vilao extends Elemento implements Serializable {

    private Random random;
    private long lastTimestamp;
    private int maxDelayToMove = 700;

    //Constroi o vilão
    public Vilao(String sNomeImagePNG) {
        super(sNomeImagePNG);
        this.random = new Random();
        int direcaoAleatoria = this.random.nextInt(4) + 1;
        this.movimentaOutros = new CapacidadeMovimento(false, true, direcaoAleatoria);
        this.bMortal = true;
        this.bTransponivel = false;
        this.lastTimestamp = 0;
        this.vilao = true;
    }

    //Volta para a última posição válida
    public void voltaAUltimaPosicao() {
        this.pPosicao.volta();
    }

    //Pega a ultima linha
    public int getLinhaAnterior() {
        return this.pPosicao.getLinhaAnterior();
    }

    //Pega a ultima coluna
    public int getColunaAnterior() {
        return this.pPosicao.getColunaAnterior();
    }

    @Override
    public boolean moveUp() {
        return this.pPosicao.setPosicao(this.pPosicao.getLinha() - 1, this.pPosicao.getColuna());
    }

    @Override
    public boolean moveDown() {
        return this.pPosicao.setPosicao(this.pPosicao.getLinha() + 1, this.pPosicao.getColuna());
    }

    @Override
    public boolean moveLeft() {
        return this.pPosicao.setPosicao(this.pPosicao.getLinha(), this.pPosicao.getColuna() - 1);
    }

    @Override
    public boolean moveRight() {
        return this.pPosicao.setPosicao(this.pPosicao.getLinha(), this.pPosicao.getColuna() + 1);
    }

    @Override
    public void mover() {
        if (this.shouldMove()) {
            int direcao = this.movimentaOutros.getDirecaoMovimento();
            if (direcao == 0) {
                direcao = 1;
            }
            Posicao proximaPosicao = Posicao.getPosicaoFromDirecao(this.pPosicao, direcao);
            while (!Posicao.ehValida(proximaPosicao)) {
                ArrayList<Integer> direcoesValidas = this.olharEmVoltaEDecidirDirecoes();
                if (direcoesValidas.size() == 0) {
                    direcao = 0;
                    break;
                }
                direcao = this.generateRandomElementFromArray(direcoesValidas);
                proximaPosicao = Posicao.getPosicaoFromDirecao(this.pPosicao, direcao);
            }
            if (direcao == 0) {
                return;
            }
            this.pPosicao = proximaPosicao;
            this.movimentaOutros.setDirecaoMovimento(direcao);

        }
    }

    private int generateRandomElementFromArray(ArrayList<Integer> array) {
        int rnd = new Random().nextInt(array.size());
        return array.get(rnd);
    }

    private ArrayList<Integer> olharEmVoltaEDecidirDirecoes() {
        int direcoes[] = {Consts.BAIXO, Consts.CIMA, Consts.ESQUERDA, Consts.DIREITA};
        ArrayList<Integer> direcoesValidas = new ArrayList<>();
        for (int i = 0; i < direcoes.length; i++) {
            int direcao = direcoes[i];
            Posicao posicaoBaseadaNaDirecao = Posicao.getPosicaoFromDirecao(this.getPosicao(), direcao);
            if (Posicao.ehValida(posicaoBaseadaNaDirecao)) {
                direcoesValidas.add(direcao);
            }
        }
        return direcoesValidas;
    }

    private boolean shouldMove() {
        long now = this.getTimeInMillis();
        if (now > this.lastTimestamp + this.maxDelayToMove) {
            this.lastTimestamp = getTimeInMillis();
            return true;
        } else {
            return false;
        }
    }
    
    private long getTimeInMillis() {
        Date date = new Date();
        long timeMilli = date.getTime();
        return timeMilli;        
    }
}
