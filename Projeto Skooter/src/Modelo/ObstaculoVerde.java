package Modelo;

import java.io.Serializable;

public class ObstaculoVerde extends Obstaculo implements Serializable{
    
    public ObstaculoVerde(String sNomeImagePNG, int id) {
        super(sNomeImagePNG, id);
        this.bTransponivel=true;
        this.bMortal=false;
    }
}
