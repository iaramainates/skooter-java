package Modelo;

public class CapacidadeMovimento {
    private boolean moveOutros; //Pode mover outros?
    private boolean seMove; //Pode se mover sozinho?
    /* Direção do Movimento
    0- Não Move
    1- Direita (+1 coluna)
    2- Esquerda (-1 coluna)
    3- Baixo (+1 linha)
    4- Cima (-1 linha)
    */
    private int direcaoMovimento;

    public CapacidadeMovimento(boolean moveOutros, boolean seMove, int direcaoMovimento) {
        this.seMove = seMove;
        this.moveOutros = moveOutros;
        this.direcaoMovimento = direcaoMovimento;
    }

    public boolean isSeMove() {
        return seMove;
    }

    public void setSeMove(boolean seMove) {
        this.seMove = seMove;
    }

    public boolean isMoveOutros() {
        return moveOutros;
    }

    public void setMoveOutros(boolean moveOutros) {
        this.moveOutros = moveOutros;
    }

    public void setDirecaoMovimento(int direcaoMovimento) {
        this.direcaoMovimento = direcaoMovimento;
    }
    
    public int getDirecaoMovimento() {
        return direcaoMovimento;
    }
}
