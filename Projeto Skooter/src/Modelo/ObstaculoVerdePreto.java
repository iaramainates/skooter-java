package Modelo;

import java.io.Serializable;

public class ObstaculoVerdePreto extends Obstaculo implements Serializable{
    
    public ObstaculoVerdePreto(String sNomeImagePNG, int id) {
        super(sNomeImagePNG, id);
        this.bTransponivel=false;
        this.bEmpurravel=true;
        this.bDestrutivel=true;
    }
    
    
}
