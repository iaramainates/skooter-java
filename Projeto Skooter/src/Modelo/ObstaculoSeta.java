package Modelo;

import java.io.Serializable;

public class ObstaculoSeta extends Obstaculo implements Serializable{
    
    public ObstaculoSeta(String sNomeImagePNG, int id) {
        super(sNomeImagePNG, id);
        this.bMortal=false;
        this.bTransponivel=true;
    }
}
