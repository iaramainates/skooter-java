package Modelo;

import Auxiliar.Posicao;

public class FactoryElemento 
{
    public static Elemento getElemento(String tipoElemento, String caminhoSprite)
    {
        if(tipoElemento.equals("Heroi"))
        {
            return new Hero(caminhoSprite);
        }
        else if(tipoElemento.equals("Vilao"))
        {
            return new Vilao(caminhoSprite);
        }
        else if(tipoElemento.equals("Bonus"))
        {
            return new Boost(caminhoSprite);
        }
        else if(tipoElemento.equals("Vermelho Simples"))
        {
            return new ObstaculoVermelho(caminhoSprite, 1);
        }
        else if(tipoElemento.equals("Vermelho Empurravel"))
        {
            return new ObstaculoVermelhoPreto(caminhoSprite, 7);
        }
        else if(tipoElemento.equals("Verde Simples"))
        {
            return new ObstaculoVerde(caminhoSprite, 8);
        }
        else if(tipoElemento.equals("Verde Empurravel"))
        {
            return new ObstaculoVerdePreto(caminhoSprite, 2);
        }
        else if(tipoElemento.contains("Seta"))
        {
            return new ObstaculoSeta(caminhoSprite, 3);
        }
        
        return null;
    }
}
