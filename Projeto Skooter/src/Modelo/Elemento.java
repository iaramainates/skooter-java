package Modelo;

import Auxiliar.Consts;
import Auxiliar.Desenhador;
import Auxiliar.Posicao;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.ImageIcon;

public abstract class Elemento implements Serializable {

    protected ImageIcon iImage;
    public Posicao pPosicao;
    protected String caminhoSprite;
    /*É elemento customizavel?*/
    protected boolean bNovo;
    /*Caminho do sprite*/
    protected boolean bTransponivel;
    /*Pode passar por cima?*/
    protected boolean bMortal;
    /*Se encostar, morre?*/
    protected boolean bEmpurravel;
    /*Pode ser empurrado pelo herói?*/
    protected boolean bBoost;
    /*Conta como ponto para avanço de fase?*/
    protected boolean bDestrutivel;
    /*Pode movimentar outros?*/
    protected CapacidadeMovimento movimentaOutros;
    
    protected boolean hero = false;
    protected boolean obstaculo = false;
    protected boolean vilao = false;

    protected Elemento(String sNomeImagePNG) {
        this.caminhoSprite = sNomeImagePNG;
        //Atribuição do elemento
        this.pPosicao = new Posicao(1, 1);
        this.bTransponivel = true;
        this.bMortal = false;
        this.bEmpurravel = false;
        this.bBoost = false;
        this.bDestrutivel = false;
        this.bNovo=false;
        this.movimentaOutros = new CapacidadeMovimento(false, false, 0);
        //Desenha o elemento
        try {
            iImage = new ImageIcon(new java.io.File(".").getCanonicalPath() + Consts.PATH + sNomeImagePNG);
            Image img = iImage.getImage();
            BufferedImage bi = new BufferedImage(Consts.CELL_SIDE, Consts.CELL_SIDE, BufferedImage.TYPE_INT_ARGB);
            Graphics g = bi.createGraphics();
            g.drawImage(img, 0, 0, Consts.CELL_SIDE, Consts.CELL_SIDE, null);
            iImage = new ImageIcon(bi);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public CapacidadeMovimento getMovimentaOutros() {
        return movimentaOutros;
    }

    public boolean isbNovo() {
        return bNovo;
    }
    
    

    public void setbMortal(boolean bMortal) {
        this.bMortal = bMortal;
    }

    public void setbEmpurravel(boolean bEmpurravel) {
        this.bEmpurravel = bEmpurravel;
    }

    public void setbBoost(boolean bBoost) {
        this.bBoost = bBoost;
    }
    
    

    public boolean isbDestrutivel() {
        return bDestrutivel;
    }

    public void setbDestrutivel(boolean bDestrutivel) {
        this.bDestrutivel = bDestrutivel;
    }

    //Pega posição
    public Posicao getPosicao() {
        return pPosicao;
    }

    //Retorna se é transponível
    public boolean isbTransponivel() {
        return bTransponivel;
    }

    //Retorna se é empurrável
    public boolean isbEmpurravel() {
        return bEmpurravel;
    }

    //Retorna se é mortal
    public boolean isbMortal() {
        return bMortal;
    }

    public boolean isbBoost() {
        return bBoost;
    }

    public boolean seMove() {
        return this.movimentaOutros.isSeMove();
    }

    //Define a habilidade de movimentar outros objetos
    public void setMovimentaOutros(boolean fazMovimentacao, int direcao) {
        this.movimentaOutros.setMoveOutros(fazMovimentacao);
        this.movimentaOutros.setDirecaoMovimento(direcao);
    }

    //Seta a capacidade de movimentar outros objetos
    public boolean moveOutros() {
        return movimentaOutros.isMoveOutros();
    }

    //Pega a direção do movimento do bloco
    public int getDirecaoMovimento() {
        return movimentaOutros.getDirecaoMovimento();
    }

    //Seta a transponilidade
    public void setbTransponivel(boolean bTransponivel) {
        this.bTransponivel = bTransponivel;
    }

    //Seta a posição
    public boolean setPosicao(int linha, int coluna) {
        return pPosicao.setPosicao(linha, coluna);
    }

    /*--Movimentos--*/
    public boolean moveUp() {
        this.movimentaOutros.setDirecaoMovimento(Consts.CIMA);
        return this.pPosicao.moveUp();
    }

    public boolean moveDown() {
        this.movimentaOutros.setDirecaoMovimento(Consts.BAIXO);
        return this.pPosicao.moveDown();
    }

    public boolean moveRight() {
        this.movimentaOutros.setDirecaoMovimento(Consts.DIREITA);
        return this.pPosicao.moveRight();
    }

    public boolean moveLeft() {
        this.movimentaOutros.setDirecaoMovimento(Consts.ESQUERDA);
        return this.pPosicao.moveLeft();
    }

    //Desenha o elemento
    public void autoDesenho() {
        Desenhador.desenhar(this.iImage, pPosicao.getColuna(), pPosicao.getLinha());
    }
    
    public void mover() {
        throw new Error("Este objeto não deveria se mover");
    }
    
    public String getCaminhoSprite() {
        return this.caminhoSprite;
    }

    public void setObstaculo(boolean obstaculo) {
        this.obstaculo = obstaculo;
    }

    public void setVilao(boolean vilao) {
        this.vilao = vilao;
    }

    
    public boolean isHero() {
        return hero;
    }

    public boolean isObstaculo() {
        return obstaculo;
    }

    public boolean isVilao() {
        return vilao;
    }    
}
