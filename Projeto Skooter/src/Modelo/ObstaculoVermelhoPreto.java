package Modelo;

import Auxiliar.Posicao;
import java.io.Serializable;

public class ObstaculoVermelhoPreto extends Obstaculo implements Serializable{
    
    public ObstaculoVermelhoPreto(String sNomeImagePNG, int id) {
        super(sNomeImagePNG, id);
        this.bTransponivel=false;
        this.bMortal=false;
        this.bEmpurravel=true;
    }
}
