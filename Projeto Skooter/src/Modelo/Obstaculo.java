package Modelo;

import java.io.Serializable;

public class Obstaculo extends Elemento implements Serializable{
    private int id;
    public Obstaculo(String sNomeImagePNG, int Id) {
        super(sNomeImagePNG);
        this.obstaculo = true;
        this.id = Id;
    }
    
    public int getId() {
        return this.id;
    }
}
