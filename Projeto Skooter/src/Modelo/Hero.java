package Modelo;

import Auxiliar.Consts;
import java.io.Serializable;

public class Hero extends Elemento implements Serializable 
{
    private int numVidas;
    private int qtdBoosts;

    //Constroi o herói
    public Hero(String sNomeImagePNG) {
        super(sNomeImagePNG);
        numVidas=0;
        qtdBoosts=0;
        this.hero = true;
    }

    public int getNumVidas() {
        return numVidas;
    }

    public void setNumVidas(int numVidas) {
        this.numVidas = numVidas;
    }

    public int getQtdBoosts() {
        return qtdBoosts;
    }

    public void setQtdBoosts(int qtdBoosts) {
        this.qtdBoosts = qtdBoosts;
    }    

    //Volta para a última posição válida
    public void voltaAUltimaPosicao() {
        this.pPosicao.volta();
    }
    
    //Pega a ultima linha
    public int getLinhaAnterior()
    {
        return this.pPosicao.getLinhaAnterior();
    }
    //Pega a ultima coluna
    public int getColunaAnterior()
    {
        return this.pPosicao.getColunaAnterior();
    }

    @Override
    public boolean moveUp() {
        this.movimentaOutros.setDirecaoMovimento(Consts.CIMA);
        return this.pPosicao.setPosicao(this.pPosicao.getLinha() - 1, this.pPosicao.getColuna());
    }

    @Override
    public boolean moveDown() {
        this.movimentaOutros.setDirecaoMovimento(Consts.BAIXO);
        return this.pPosicao.setPosicao(this.pPosicao.getLinha() + 1, this.pPosicao.getColuna());
    }

    @Override
    public boolean moveLeft() {
        this.movimentaOutros.setDirecaoMovimento(Consts.ESQUERDA);
        return this.pPosicao.setPosicao(this.pPosicao.getLinha(), this.pPosicao.getColuna() - 1);
    }

    @Override
    public boolean moveRight() {
        this.movimentaOutros.setDirecaoMovimento(Consts.DIREITA);
        return this.pPosicao.setPosicao(this.pPosicao.getLinha(), this.pPosicao.getColuna() + 1);
    }
}
