package Modelo;

import java.io.Serializable;

public class Boost extends Elemento implements Serializable{
    //Constroi o elemento
    public Boost(String sNomeImagePNG) {
        super(sNomeImagePNG);
        this.bTransponivel = true;
        this.bBoost=true;
    }
}
