package Fases;

import Auxiliar.FaseXmlDomParser;
import Modelo.Elemento;
import Modelo.Hero;
import java.util.ArrayList;

public class ControladorDeFase {

    private static ControladorDeFase singleton;
    private int faseAtual;
    private ArrayList<Fase> fases;

    private ControladorDeFase() {
        singleton = this;
        fases = new ArrayList<>(4);
        faseAtual = -1;
    }

    //Pega o herói
    public Hero getHero() {
        if (this.faseAtual != -1) {
            return this.fases.get(this.faseAtual).getHero();
        } else {
            throw new Error("Tentativa de recarregar fase não criada");
        }
    }
    
    public ArrayList<Elemento> getElements()
    {
        if (this.faseAtual != -1) {
            return this.fases.get(this.faseAtual).getElementos();
        } else {
            throw new Error("Tentativa de recarregar fase não criada");
        }
    }

    //Pega Fase Atual
    public Fase getFaseAtual() {
        return this.fases.get(faseAtual);
    }

    //Pega o Número da Fase Atual
    public int getNumeroFase() {
        return this.faseAtual;
    }

    //Avança de fase
    public Fase proximaFase(String FileName, String local) {
        try {
            ControladorDeColisao.getSingleton().limpaPosicoes();
            Fase fase = this.carregarFase(local + "/" + FileName);
            this.faseAtual++;
            this.fases.add(fase);
            return fase;
        } catch (Error _erro) {
            ControladorDeColisao.getSingleton().limpaPosicoes();
            Fase fase = this.carregarFase("config/FaseFinal.xml");
            this.faseAtual++;
            this.fases.add(fase);
            return fase;
        }        
    }

    //Recarrega a Fase
    public Fase recarregarFase() {
        if (this.faseAtual != -1) {
            ControladorDeColisao.getSingleton().limpaPosicoes();
            String Filename = this.fases.get(faseAtual).FileName;
            this.fases.set(this.faseAtual, this.carregarFase(Filename));
            return this.fases.get(this.faseAtual);
        } else {
            throw new Error("Tentativa de recarregar fase não criada");
        }
    }

    //Carrega a Fase
    public Fase carregarFase(String FileName) {
        Fase fase = FaseXmlDomParser.parse(FileName);
        return fase;
    }

    public static ControladorDeFase getSingleton() 
    {
        if(singleton==null)
        {
            singleton = new ControladorDeFase();
        }
        return singleton;
    }
}
