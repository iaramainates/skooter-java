/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fases;

import Auxiliar.Posicao;
import Modelo.Elemento;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ellia
 */
public class ControladorDeColisao {

    private static ControladorDeColisao singleton;
    private int numeroDeLinhas;
    private int numeroDeColunas;
    private Elemento[][] matrizDeColisoes;

    public ControladorDeColisao(int numeroDeLinhas, int numeroDeColunas) {
        singleton = this;
        this.numeroDeLinhas = numeroDeLinhas;
        this.numeroDeColunas = numeroDeColunas;
        this.matrizDeColisoes = new Elemento[numeroDeLinhas][numeroDeColunas];
    }

    public boolean espacoOcupado(Posicao p) {
        return (this.matrizDeColisoes[p.getLinha()][p.getColuna()] != null);
    }

    public void ocuparEspaco(Posicao p, Elemento e) {
        this.matrizDeColisoes[p.getLinha()][p.getColuna()]  = e;
    }

    public void desocuparEspaco(Posicao p) {
        this.matrizDeColisoes[p.getLinha()][p.getColuna()] = null;
    }

    public void limpaPosicoes() {
        for (int i = 0; i < this.matrizDeColisoes.length; i++) {
            Arrays.fill(this.matrizDeColisoes[i], null);
        }
    }
    
    public static ControladorDeColisao getSingleton() {
        return singleton;
    }

    public Elemento getElementoFromPosicao(Posicao posicaoDoElemento) {
        return this.matrizDeColisoes[posicaoDoElemento.getLinha()][posicaoDoElemento.getColuna()];
    }
}
