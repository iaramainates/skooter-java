package Fases;

import Modelo.Elemento;
import Modelo.Hero;
import java.util.ArrayList;

public class Fase {

    //Elementos da Fase
    private final ArrayList<Elemento> elementosDesenhar;
    //Nome do arquivo XML
    protected final String FileName;
    //Herói da Fase
    public Hero hero;
    //Caminho do Background
    public String caminhoBackground;
    //Quantidade de boosts para ganhar
    private int boostsParaTerminar;

    //Constrói a Fase
    public Fase(String FileName) {
        this.elementosDesenhar = new ArrayList<>();
        this.FileName = FileName;
    }

    //Pega o Heroi
    public Hero getHero() {
        return this.hero;
    }

    //Adiciona um Elemento a Fase
    public void addElemento(Elemento elemento) {
        if (this.elementosDesenhar.isEmpty()) {
            this.hero = (Hero) elemento;
            this.elementosDesenhar.add(elemento);
        } else {
            this.elementosDesenhar.add(elemento);
            ControladorDeColisao.getSingleton().ocuparEspaco(elemento.getPosicao(), elemento);
        }
    }

    //Remove um Elemento a Fase
    public void removeElemento(Elemento elementoASerRemovido) {
        ControladorDeColisao.getSingleton().desocuparEspaco(elementoASerRemovido.getPosicao());
        this.elementosDesenhar.remove(elementoASerRemovido);
    }

    //Obtem Elementos da Fase
    public ArrayList<Elemento> getElementos() {
        return this.elementosDesenhar;
    }

    //Seta o caminho do  background
    public void setCaminhoBackground(String caminhoBackground) {
        this.caminhoBackground = caminhoBackground;
    }

    //Obtem caminho do background
    public String getCaminhoBackground() {
        return caminhoBackground;
    }

    public int getBoostsParaTerminar() {
        return boostsParaTerminar;
    }

    public void setBoostsParaTerminar(int boostsParaTerminar) {
        this.boostsParaTerminar = boostsParaTerminar;
    }
}
