package Controler;

import Auxiliar.*;
import Fases.ControladorDeColisao;

import Fases.ControladorDeFase;
import Modelo.Elemento;

import java.awt.*;
import java.awt.event.*;

import java.io.*;

import java.util.*;
import java.util.logging.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

//Tela com suporte para teclado
public class Tela extends javax.swing.JFrame implements MouseListener,KeyListener {

    private ControleDeJogo cControle = new ControleDeJogo();

    //Gerencia gráficos (não iremos utilizar
    private Graphics g2;
    
    private ProxyControleMovimento filtroControles;

    /**
     * Cria uma nova tela
     */
    public Tela() {
        Desenhador.setCenario(this); //Desenhador funciona no modo estatico
        initComponents();

        this.addMouseListener(this);
        this.addKeyListener(this); //Teclado

        /*Cria a janela do tamanho do cenario + insets (bordas) da janela*/
        this.setSize(Consts.RES * Consts.CELL_SIDE + getInsets().left + getInsets().right,
                Consts.RES * Consts.CELL_SIDE + getInsets().top + getInsets().bottom);

        System.out.println("Yay, seja bem-vindo!");
        ControladorDeFase.getSingleton().proximaFase("Fase0.xml", "config");
        
        this.filtroControles = new ProxyControleMovimento();
    }

    /*--------------------------------------------------*/
    //Controles de adição e remoção de elementos
    public Graphics getGraphicsBuffer() {
        return g2;
    }

    /*--------------------------------------------------*/

 /*Este metodo é executado a cada Consts.FRAME_INTERVAL milissegundos, reconstrói a tela*/
    public void paint(Graphics gOld) {

        Graphics g = this.getBufferStrategy().getDrawGraphics();

        /*Criamos um contexto gráfico*/
        g2 = g.create(getInsets().left, getInsets().top, getWidth() - getInsets().right, getHeight() - getInsets().top);

        /*Desenha cenário*/
        for (int i = 0; i < Consts.RES; i++) {
            for (int j = 0; j < Consts.RES; j++) {
                try {
                    /*Linha para alterar o background*/
                    String caminhoImg = ControladorDeFase.getSingleton().getFaseAtual().getCaminhoBackground();
                    Image newImage = Toolkit.getDefaultToolkit().getImage(new java.io.File(".").getCanonicalPath() + Consts.PATH + caminhoImg);
                    g2.drawImage(newImage, j * Consts.CELL_SIDE, i * Consts.CELL_SIDE, Consts.CELL_SIDE, Consts.CELL_SIDE, null);

                } catch (IOException ex) {
                    Logger.getLogger(Tela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        /*Aqui podem ser inseridos novos processamentos de controle*/
        if (!ControladorDeFase.getSingleton().getFaseAtual().getElementos().isEmpty()) {
            this.cControle.desenhaTudo(ControladorDeFase.getSingleton().getFaseAtual().getElementos());
            this.cControle.processaTudo(ControladorDeFase.getSingleton().getFaseAtual().getElementos());

            if (ControladorDeFase.getSingleton().getFaseAtual().getBoostsParaTerminar() == 0 
                    && ControladorDeFase.getSingleton().getNumeroFase() != 0) {
                System.out.println("Você passou para a próxima fase!");
                int fase = ControladorDeFase.getSingleton().getNumeroFase();
                fase = fase + 2;
                ControladorDeFase.getSingleton().proximaFase("Fase" + fase + ".xml", "config");
            }
        }

        g.dispose();
        g2.dispose();
        if (!getBufferStrategy().contentsLost()) {
            getBufferStrategy().show();
        }
    }

    //Gerencia a execução do jogo
    public void go() {
        TimerTask redesenhar = new TimerTask() {
            public void run() {
                repaint();
                /*(executa o metodo paint)*/
            }
        };

        /*Redesenha (executa o metodo paint) tudo a cada Consts.FRAME_INTERVAL milissegundos*/
        Timer timer = new Timer();
        timer.schedule(redesenhar, 0, Consts.FRAME_INTERVAL);
    }

    //Pega eventos de teclas pressionadas
    public void keyPressed(KeyEvent e) {
        /*Movimento do heroi via teclado*/
        this.filtroControles.controlaMovimento(e);
        
        /*Se o heroi for para uma posicao invalida, sobre um elemento intransponivel, volta para onde estava*/
        if (!cControle.ehPosicaoValida(ControladorDeFase.getSingleton().getFaseAtual().getElementos(), ControladorDeFase.getSingleton().getHero().getPosicao())) {
            ControladorDeFase.getSingleton().getHero().voltaAUltimaPosicao();
        }

        this.setTitle("-> Cell: " + (ControladorDeFase.getSingleton().getHero().getPosicao().getLinha()) + ", " + (ControladorDeFase.getSingleton().getHero().getPosicao().getColuna()));
    }
    
    public void mousePressed(MouseEvent e) 
    {
         //Movimento via mouse
         int x = e.getX();
         int y = e.getY();
         
         //String nome do arquivo
         String nomeArquivo;
         
         boolean flag=false;
         
         if(e.getButton() == MouseEvent.BUTTON3)
         {
            x=x/Consts.CELL_SIDE;
            y=y/Consts.CELL_SIDE;
            //Posicao do Elemento
            Posicao posicaoElemento = new Posicao(x,y-1);
            
            //Abrindo o arquivo
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "XML Archives", "xml");
            chooser.setFileFilter(filter);
            
            int returnVal = chooser.showOpenDialog(this);
            if(returnVal == JFileChooser.APPROVE_OPTION) 
            {
                try 
                {
                    nomeArquivo = chooser.getSelectedFile().getCanonicalPath();
                    
                    //cControle.substituiElemento(nomeArquivo,posicaoElemento);
                    Elemento elemento = FaseXmlDomParser.parseElement(nomeArquivo);

                    ArrayList<Elemento> elementos = ControladorDeFase.getSingleton().getElements();
                    Elemento elementoAtual;

                    for(int i=0;i<elementos.size();i++)
                    {
                        elementoAtual = elementos.get(i);

                        if(elementoAtual.pPosicao.getColuna()==x && elementoAtual.pPosicao.getLinha()==y-1)
                        {
                            elemento.setPosicao(y-1, x);
                            ControladorDeFase.getSingleton().getElements().remove(i);
                            ControladorDeFase.getSingleton().getElements().add(elemento);
                            this.repaint();
                        }

                    }
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(Tela.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else
            {
                System.out.println("Nao achamos o arquivo :(");
            }
         }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("POO2021");
        setAutoRequestFocus(false);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 561, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent me) {}

    @Override
    public void mouseReleased(MouseEvent me) {}

    @Override
    public void mouseEntered(MouseEvent me) {}

    @Override
    public void mouseExited(MouseEvent me) {}
}
