package Controler;

import Auxiliar.Consts;
import Auxiliar.FaseXmlDomParser;
import Modelo.Elemento;
import Modelo.Hero;
import Auxiliar.Posicao;
import Fases.ControladorDeColisao;
import Fases.ControladorDeFase;
import Modelo.ElementoNovo;
import java.util.ArrayList;

public class ControleDeJogo {

    //Desenha todos os elementos
    public void desenhaTudo(ArrayList<Elemento> e) {
        for (int i = 0; i < e.size(); i++) {
            e.get(i).autoDesenho();
        }
    }

    //Processa todos os elementos e suas colisões
    public void processaTudo(ArrayList<Elemento> e) {
        Hero hHero = (Hero) e.get(0);
        /*O heroi (protagonista) eh sempre o primeiro do array*/

        Elemento eTemp;

        /*Processa todos os demais em relacao ao heroi*/
        for (int i = 1; i < e.size(); i++) {
            eTemp = e.get(i);
            /*Pega o i-esimo elemento do jogo*/
            ControladorDeColisao.getSingleton().desocuparEspaco(eTemp.getPosicao());
            if (eTemp.seMove()) {
                eTemp.mover();
            }
            ControladorDeColisao.getSingleton().ocuparEspaco(eTemp.getPosicao(), eTemp);

            // Verifica se o heroi se sobrepoe ao i-ésimo elemento
            if (hHero.getPosicao().estaNaMesmaPosicao(eTemp.getPosicao())) {
                /*Nem todos os elementos podem ser transpostos pelo heroi*/
                if (eTemp.isbTransponivel()) {
                    this.lidaComObjetoTransponivel(eTemp, hHero, e);
                } else if (eTemp.isbEmpurravel()) {
                    this.lidaComObjetoEmpurravel(eTemp, hHero);
                }

                if (eTemp.isbMortal()) {
                    int qtdVidas = hHero.getNumVidas();

                    hHero.setNumVidas(qtdVidas - 1);
                    System.out.println("Atenção com os robôs!");

                    if (hHero.getNumVidas() <= 0) {
                        System.out.println("Fim de Jogo! Você perdeu :c");
                        int fase = ControladorDeFase.getSingleton().getNumeroFase();
                        ControladorDeFase.getSingleton().proximaFase("FaseFinal.xml", "config");
                        // Espera um tempo pra fechar o jogo
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                            @Override
                            public void run() {
                                System.exit(0);
                            }
                        },
                                2000
                        );
                    }
                }

                if (eTemp.isbBoost()) {
                    int qtdBoosts = hHero.getQtdBoosts();
                    int boostsParaTerminar = ControladorDeFase.getSingleton().getFaseAtual().getBoostsParaTerminar();
                    System.out.println("Você coletou um boost! Faltam " + (boostsParaTerminar - 1) + " para a próxima fase!");
                    hHero.setQtdBoosts(qtdBoosts + 1);
                    ControladorDeFase.getSingleton().getFaseAtual().setBoostsParaTerminar(boostsParaTerminar - 1);
                }
            }
        }
    }

    //Verifica se a posição é valida
    public boolean ehPosicaoValida(ArrayList<Elemento> e, Posicao p) {
        Elemento eTemp;

        /*Validacao da posicao de todos os elementos com relacao a Posicao p*/
        for (int i = 1; i < e.size(); i++) {
            /*Olha todos os elementos*/
            eTemp = e.get(i);
            /*Pega o i-esimo elemento do jogo*/

            if (!eTemp.isbTransponivel()) {
                if (eTemp.getPosicao().estaNaMesmaPosicao(p)) {
                    if (eTemp.isbEmpurravel() || eTemp.isbMortal() || eTemp.isbBoost()) {
                        //Se ele estiver em cima de um objeto mas for empurrável ou mortal ou boost
                        return true;
                    } else {
                        return false;
                    }
                    /*A posicao p é invalida, pois ha um elemento (i-esimo eTemp) intransponivel lá*/
                }
            }
        }
        return true;
    }

    private void lidaComObjetoEmpurravel(Elemento eTemp, Hero hHero) {
        //Se for empurrável

        int direcaoHeroi = hHero.getDirecaoMovimento();
        //Empurra de acordo com a posição anterior
        Posicao novaPosicaoDoElemento = Posicao.getPosicaoFromDirecao(eTemp.pPosicao, direcaoHeroi);
        boolean ehPosicaoValida = Posicao.ehValida(novaPosicaoDoElemento);

        if (ehPosicaoValida) {
            ControladorDeColisao.getSingleton().desocuparEspaco(eTemp.getPosicao());
            eTemp.setPosicao(novaPosicaoDoElemento.getLinha(), novaPosicaoDoElemento.getColuna());
            ControladorDeColisao.getSingleton().ocuparEspaco(eTemp.getPosicao(), eTemp);
        } else {
            hHero.voltaAUltimaPosicao();
        }
    }

    private void lidaComObjetoTransponivel(Elemento eTemp, Hero hHero, ArrayList<Elemento> e) {
        //Se ele é transponível e move outros
        if (eTemp.moveOutros()) {
            int direcaoEmpurra = eTemp.getDirecaoMovimento();
            hHero.pPosicao = Posicao.getPosicaoFromDirecao(hHero.pPosicao, direcaoEmpurra);
        } else {
            ControladorDeColisao.getSingleton().desocuparEspaco(eTemp.getPosicao());
            e.remove(eTemp);
        }
    }

}
