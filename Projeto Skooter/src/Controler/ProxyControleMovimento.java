/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Auxiliar.FaseXmlDomParser;
import Auxiliar.FaseXmlDomWriter;
import Auxiliar.Posicao;
import Fases.ControladorDeColisao;
import Fases.ControladorDeFase;
import Fases.Fase;
import Modelo.Elemento;
import java.awt.List;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 *
 * @author iaraduarte
 */
public class ProxyControleMovimento 
{
    private ArrayList<Integer> eventosPermitidos;

    public ProxyControleMovimento() {
        
        this.eventosPermitidos = new ArrayList();
        eventosPermitidos.add(KeyEvent.VK_UP);
        eventosPermitidos.add(KeyEvent.VK_DOWN);
        eventosPermitidos.add(KeyEvent.VK_LEFT);
        eventosPermitidos.add(KeyEvent.VK_RIGHT);
        eventosPermitidos.add(KeyEvent.VK_SPACE);
        eventosPermitidos.add(KeyEvent.VK_R);
        eventosPermitidos.add(KeyEvent.VK_P);
        eventosPermitidos.add(KeyEvent.VK_Q);
        eventosPermitidos.add(KeyEvent.VK_S);
        eventosPermitidos.add(KeyEvent.VK_C);
    }
    
    public void iniciaTeclasPermitidas()
    {
        
    }
    
    public void controlaMovimento(KeyEvent e)
    {
        if(this.eventosPermitidos.contains(e.getKeyCode()))
        {
            switch (e.getKeyCode()) 
            {
                case KeyEvent.VK_UP:
                    ControladorDeFase.getSingleton().getHero().moveUp();
                    break;
                case KeyEvent.VK_DOWN:
                    ControladorDeFase.getSingleton().getHero().moveDown();
                    break;
                case KeyEvent.VK_LEFT:
                    ControladorDeFase.getSingleton().getHero().moveLeft();
                    break;
                case KeyEvent.VK_RIGHT:
                    ControladorDeFase.getSingleton().getHero().moveRight();
                    break;
                case KeyEvent.VK_SPACE:
                    // Tenta Quebrar o bloco
                    int direcaoHeroi = ControladorDeFase.getSingleton().getHero().getDirecaoMovimento();
                    Posicao posicaoHeroi = ControladorDeFase.getSingleton().getHero().getPosicao();
                    ArrayList<Elemento> elementos = ControladorDeFase.getSingleton().getFaseAtual().getElementos();
                    Posicao posicaoNaFrenteDoHeroi = Posicao.getPosicaoFromDirecao(posicaoHeroi, direcaoHeroi);
                    Elemento elemento = ControladorDeColisao.getSingleton().getElementoFromPosicao(posicaoNaFrenteDoHeroi);
                    if (elemento != null && elemento.isbDestrutivel()) {
                        ControladorDeColisao.getSingleton().desocuparEspaco(elemento.getPosicao());
                        elementos.remove(elemento);
                    }
                    break;
                case KeyEvent.VK_R:
                    //Reload
                    ControladorDeFase.getSingleton().recarregarFase();
                    //Restaurar Vidas
                    ControladorDeFase.getSingleton().getFaseAtual().getHero().setNumVidas(5);
                    ControladorDeFase.getSingleton().getFaseAtual().getHero().setQtdBoosts(0);
                    break;
                case KeyEvent.VK_P:
                    // Próxima Fase
                    int fase = ControladorDeFase.getSingleton().getNumeroFase();
                    fase = fase + 1;
                    System.out.println("Fase = " + fase);
                    ControladorDeFase.getSingleton().proximaFase("Fase" + fase + ".xml", "config");
                    break;
                case KeyEvent.VK_S:
                    FaseXmlDomWriter.saveToXml(ControladorDeFase.getSingleton().getFaseAtual(), ControladorDeFase.getSingleton().getNumeroFase());
                    System.exit(0);
                    break;
                case KeyEvent.VK_C:
                    if (ControladorDeFase.getSingleton().getNumeroFase() == 0) {
                        ControladorDeFase.getSingleton().proximaFase("unamed_save.xml", "autosaves");
                    }
                    break;
                case KeyEvent.VK_Q:
                    // Fecha o Jogo
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
        else
        {
            System.out.println("Evento não permitido!");
        }
    }
}
