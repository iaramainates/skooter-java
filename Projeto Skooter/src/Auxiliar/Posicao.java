package Auxiliar;

import Fases.ControladorDeColisao;
import Modelo.Elemento;
import java.io.Serializable;

public class Posicao implements Serializable {

    private int linha;
    private int coluna;

    private int linhaAnterior;
    private int colunaAnterior;

    //Constroi a posição
    public Posicao(int linha, int coluna) {
        this.linha = linha;
        this.coluna = coluna;
    }

    //Determina uma nova posição
    public boolean setPosicao(int linha, int coluna) {
        //Se a linha estiver fora do frame
        if (linha < 0 || linha > Auxiliar.Consts.RES - 1) {
            return false; //Não é possível mover para essa linha
        }

        //Se a coluna estiver fora do frame
        if (coluna < 0 || coluna > Auxiliar.Consts.RES - 1) {
            return false; // Não é possível mover para essa coluna
        }

        linhaAnterior = this.linha;
        this.linha = linha;
        colunaAnterior = this.coluna;
        this.coluna = coluna;
        return true;
    }

    //Obtem linha
    public int getLinha() {
        return linha;
    }

    //Obtem coluna
    public int getColuna() {
        return coluna;
    }

    //Obtem linha anterior
    public int getLinhaAnterior() {
        return linhaAnterior;
    }

    //Obtem coluna anterior
    public int getColunaAnterior() {
        return colunaAnterior;
    }

    //Volta uma posição
    public boolean volta() {
        return this.setPosicao(linhaAnterior, colunaAnterior);
    }

    //Verifica se dois objetos estão na mesma posição
    public boolean estaNaMesmaPosicao(Posicao posicao) {
        return (linha == posicao.getLinha() && coluna == posicao.getColuna());
    }

    //Copia uma posição
    public boolean copia(Posicao posicao) {
        return this.setPosicao(posicao.getLinha(), posicao.getColuna());
    }

    /*--Movimentações--*/
    //Move para cima
    public boolean moveUp() {
        return this.setPosicao(this.getLinha() - 1, this.getColuna());
    }

    //Move para baixo
    public boolean moveDown() {
        return this.setPosicao(this.getLinha() + 1, this.getColuna());
    }

    //Move para direita
    public boolean moveRight() {
        return this.setPosicao(this.getLinha(), this.getColuna() + 1);
    }

    //Move para a esquerda
    public boolean moveLeft() {
        return this.setPosicao(this.getLinha(), this.getColuna() - 1);
    }

    static public Posicao getPosicaoFromDirecao(Posicao posicaoAtual, int direcaoMovimento) {
        switch (direcaoMovimento) {
            case Consts.DIREITA:
                return new Posicao(posicaoAtual.getLinha(), posicaoAtual.getColuna() + 1);
            case Consts.ESQUERDA:
                return new Posicao(posicaoAtual.getLinha(), posicaoAtual.getColuna() - 1);
            case Consts.CIMA:
                return new Posicao(posicaoAtual.getLinha() - 1, posicaoAtual.getColuna());
            case Consts.BAIXO:
                return new Posicao(posicaoAtual.getLinha() + 1, posicaoAtual.getColuna());
            default:
                return posicaoAtual;
        }
    }

    static public boolean ehValida(Posicao posicaoParaVerificar) {
        int linha = posicaoParaVerificar.getLinha();
        int coluna = posicaoParaVerificar.getColuna();
        //Se a linha estiver fora do frame
        if (linha < 0 || linha > Auxiliar.Consts.RES - 1) {
            return false; //Não é possível mover para essa linha
        }

        //Se a coluna estiver fora do frame
        if (coluna < 0 || coluna > Auxiliar.Consts.RES - 1) {
            return false; // Não é possível mover para essa coluna
        }

        if (ControladorDeColisao.getSingleton().espacoOcupado(posicaoParaVerificar)) {
            return false;
        }

        return true;
    }
}
