package Auxiliar;

import Fases.Fase;

import Modelo.Boost;
import Modelo.Elemento;
import Modelo.ElementoNovo;
import Modelo.FactoryElemento;
import Modelo.Hero;
import Modelo.Obstaculo;
import Modelo.ObstaculoSeta;
import Modelo.ObstaculoVerde;
import Modelo.ObstaculoVerdePreto;
import Modelo.ObstaculoVermelho;
import Modelo.ObstaculoVermelhoPreto;
import Modelo.Vilao;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public final class FaseXmlDomParser {

    public static Elemento parseElement(String FILENAME)
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Elemento elementoExemplo;
        
        try {
            //Parseia XML
            DocumentBuilder db = dbf.newDocumentBuilder();
            File f = new File(FILENAME);
            Document doc = db.parse(f);
            NodeList list;
            Node node;
                       
            list = doc.getElementsByTagName("elemento");
            node = list.item(0);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) 
            {
                Element element = (Element) node;

                elementoExemplo = FaseXmlDomParser.getElementoFromElement(element);
                
            } else {
                throw new Error("Objeto não encontrado");
            }
            
            return elementoExemplo;
            
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println("Erro ao carregar XML");
            System.err.println(e.getMessage());
            throw new Error("Erro no XML");
        }
    }
    
    //Parseia o XML e constrói a fase
    public static Fase parse(String FILENAME) {

        //Instancia Factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            Fase fase = new Fase(FILENAME);

            //Parseia XML
            DocumentBuilder db = dbf.newDocumentBuilder();
            File f = new File(FILENAME);
            Document doc = db.parse(f);
            NodeList list;
            Node node;
            
            //Pegando Background
            Node faseNode = doc.getElementsByTagName("fase").item(0);
            int numeroDeBoosts = Integer.parseInt(((Element)faseNode).getAttribute("numeroDeBoosts"));
            fase.setBoostsParaTerminar(numeroDeBoosts);
            
            //Pegando Background
            NodeList listBack=doc.getElementsByTagName("background");
            
            for(int i=0;i<listBack.getLength();i++)
            {
                node=listBack.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    
                    String caminho = FaseXmlDomParser.getBackground(element);
                    
                    fase.setCaminhoBackground(caminho);

                } else {
                    throw new Error("Caminho de Background não encontrado");
                }
            }
            

            //Pegando Jogador
            list = doc.getElementsByTagName("jogador");
            node = list.item(0);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                Hero heroi = FaseXmlDomParser.getHeroFromElement(element);
                fase.addElemento(heroi);
                
            } else {
                throw new Error("Heroi não encontrado");
            }
            
            //Pegando Vilões
            NodeList listVil=doc.getElementsByTagName("vilao");
            for(int i=0;i<listVil.getLength();i++)
            {
                node=listVil.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    
                    Elemento obs = FaseXmlDomParser.loadVilains(element);
                    fase.addElemento(obs);

                } else {
                    throw new Error("Vilão não encontrado");
                }
            }
            
            
            //Pegando Obstáculos
            NodeList listObs=doc.getElementsByTagName("obstaculo");
            
            for(int i=0;i<listObs.getLength();i++)
            {
                node=listObs.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    
                    Obstaculo obs = FaseXmlDomParser.loadObstacles(element);
                    fase.addElemento(obs);

                } else {
                    throw new Error("Obstaculo não encontrado");
                }
            }            
            
            //Construindo boosts
            NodeList listBoost=doc.getElementsByTagName("fruta");
            
            for(int i=0;i<listBoost.getLength();i++)
            {
                node=listBoost.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    
                    Boost boost = FaseXmlDomParser.loadBoost(element);
                    
                    fase.addElemento(boost);

                } else {
                    throw new Error("Fruta não encontrada");
                }
            }
            
            return fase;
            
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println("Erro ao carregar XML");
            System.err.println(e.getMessage());
            throw new Error("Erro no XML");
        }
    }

    static Elemento getElementoFromElement(Element element)
    {
        String caminhoSprite = element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        ElementoNovo elementoNovo = new ElementoNovo(caminhoSprite);
        
        int transponivel,mortal,empurravel,boost,hero,vilao,obstaculo,destrutivel,movimentaOutros;
        
        transponivel = Integer.parseInt(element.getElementsByTagName("transponivel").item(0).getTextContent());
        mortal = Integer.parseInt(element.getElementsByTagName("mortal").item(0).getTextContent());
        empurravel = Integer.parseInt(element.getElementsByTagName("empurravel").item(0).getTextContent());
        boost = Integer.parseInt(element.getElementsByTagName("boost").item(0).getTextContent());
        hero = Integer.parseInt(element.getElementsByTagName("hero").item(0).getTextContent());
        vilao = Integer.parseInt(element.getElementsByTagName("vilao").item(0).getTextContent());
        obstaculo = Integer.parseInt(element.getElementsByTagName("obstaculo").item(0).getTextContent());
        destrutivel = Integer.parseInt(element.getElementsByTagName("destrutivel").item(0).getTextContent());
        movimentaOutros = Integer.parseInt(element.getElementsByTagName("movimentaOutros").item(0).getTextContent());
        
        elementoNovo.setbTransponivel(false);
        elementoNovo.setbMortal(false);
        elementoNovo.setbEmpurravel(false);
        elementoNovo.setbBoost(false);
        elementoNovo.setbDestrutivel(false);
        elementoNovo.setMovimentaOutros(false,0);
        
        if(transponivel==1)
        {
            elementoNovo.setbTransponivel(true);
        }
        if(mortal==1)
        {
            elementoNovo.setbMortal(true);
        }
        if(empurravel==1)
        {
            elementoNovo.setbEmpurravel(true);
        }
        if(boost==1)
        { 
            elementoNovo.setbBoost(true);
        }
        if(vilao==1)
        { 
            elementoNovo.setVilao(true);
        }
        if(obstaculo==1)
        { 
            elementoNovo.setObstaculo(true);
        }
        if(destrutivel==1)
        {
            elementoNovo.setbDestrutivel(true);
        }
        if(movimentaOutros==1)
        {
            int direcaoMovimento = Integer.parseInt(element.getElementsByTagName("direcaoMovimento").item(0).getTextContent());
            elementoNovo.setMovimentaOutros(true, direcaoMovimento);
        }
        
        return elementoNovo;
    }
    
    //Pega Background
    static String getBackground(Element element)
    {
        String caminho=element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        return caminho;
    }
    
    //Carrega Herói
    static Hero getHeroFromElement(Element element) {
        Integer linhaInicial = Integer.parseInt(element.getAttribute("linhaInicial"));
        Integer colunaInicial = Integer.parseInt(element.getAttribute("colunaInicial"));

        String caminhoSprite = element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        int vidas = Integer.parseInt(element.getElementsByTagName("vidas").item(0).getTextContent());
        
        Hero heroi = (Hero)FactoryElemento.getElemento("Heroi", caminhoSprite);
        heroi.setPosicao(linhaInicial, colunaInicial);
        heroi.setNumVidas(vidas+heroi.getNumVidas());
        return heroi;
    }

    //Carrega Vilões
    static Elemento loadVilains(Element element)
    {
        Integer linhaInicial = Integer.parseInt(element.getAttribute("linhaInicial"));
        Integer colunaInicial = Integer.parseInt(element.getAttribute("colunaInicial"));
        String caminhoSprite = element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        
        Vilao vilao = (Vilao)FactoryElemento.getElemento("Vilao", caminhoSprite);
        
        vilao.setPosicao(linhaInicial, colunaInicial);
        vilao.setbTransponivel(false);
            
        return vilao;
    }
    
    //Carrega Obstaculos
    static Obstaculo loadObstacles(Element element) 
    {
        Integer linhaInicial = Integer.parseInt(element.getAttribute("linhaInicial"));
        Integer colunaInicial = Integer.parseInt(element.getAttribute("colunaInicial"));
        
        Integer id=Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent());
        
        String caminhoSprite = element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        
        switch (id) 
        {
            case 1:
            {
                //Obstaculos Vermelhos Imóveis
                ObstaculoVermelho obstaculo = (ObstaculoVermelho)FactoryElemento.getElemento("Vermelho Simples", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                
                return obstaculo;
            }
            case 2:
            {
                //Obstáculos Verdes Empurráveis
                ObstaculoVerdePreto obstaculo = (ObstaculoVerdePreto)FactoryElemento.getElemento("Verde Empurravel", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                return obstaculo;
            }
            case 3:
            {
                //Obstáculo →
                ObstaculoSeta obstaculo = (ObstaculoSeta)FactoryElemento.getElemento("Seta", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                obstaculo.setMovimentaOutros(true, 1);
                return obstaculo;
            }
            case 4:
            {
                //Obstáculo ←
                ObstaculoSeta obstaculo = (ObstaculoSeta)FactoryElemento.getElemento("Seta", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                obstaculo.setMovimentaOutros(true, 2);
                return obstaculo;
            }
            case 5:
            {
                //Obstáculo ↑
                ObstaculoSeta obstaculo = (ObstaculoSeta)FactoryElemento.getElemento("Seta", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                obstaculo.setMovimentaOutros(true, 4);
                return obstaculo;
            }
            case 6:
            {
                //Obstáculo ↓
                ObstaculoSeta obstaculo = (ObstaculoSeta)FactoryElemento.getElemento("Seta", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                obstaculo.setMovimentaOutros(true, 3);
                return obstaculo;
            }
            case 7:
            {
                //Obstáculo Vermelho Empurrável
                ObstaculoVermelhoPreto obstaculo = (ObstaculoVermelhoPreto)FactoryElemento.getElemento("Vermelho Empurravel", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                return obstaculo;
            }
            case 8:
            {
                //Obstáculo Verde Consumível
                ObstaculoVerde obstaculo= (ObstaculoVerde)FactoryElemento.getElemento("Verde Simples", caminhoSprite);
                obstaculo.setPosicao(linhaInicial, colunaInicial);
                return obstaculo;
            }
            default:
                break;
        }
        
        return null;
    }
    
    //Carrega Boosts
    static Boost loadBoost(Element element) 
    {
        Integer linhaInicial = Integer.parseInt(element.getAttribute("linhaInicial").toString());
        Integer colunaInicial = Integer.parseInt(element.getAttribute("colunaInicial").toString());
        
        String caminhoSprite = element.getElementsByTagName("caminhoSprite").item(0).getTextContent();
        
        Boost boost = (Boost)FactoryElemento.getElemento("Bonus", caminhoSprite);
        boost.setPosicao(linhaInicial, colunaInicial);
        return boost;
    }
}
