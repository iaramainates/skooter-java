package Auxiliar;

import java.awt.Graphics;
import java.io.Serializable;
import javax.swing.ImageIcon;
import Controler.Tela;

public class Desenhador implements Serializable {
 
    static Tela tCenarioOndeSeDesenha;
    
    //Desenha o Cenario
    public static void setCenario(Tela umTCenarioOndeDesenhar) {
        tCenarioOndeSeDesenha = umTCenarioOndeDesenhar;
    }

    
    //Desenha a tela
    public static Tela getTelaDoJogo() {
        return tCenarioOndeSeDesenha;
    }
    
    //Desenha um elemento
    public static void desenhar(ImageIcon iImage, int iColuna, int iLinha) {
        iImage.paintIcon(tCenarioOndeSeDesenha,getGraphicsDaTela(),iColuna * Consts.CELL_SIDE, iLinha * Consts.CELL_SIDE);
    }
    
    //Pega os gráficos da tela
    /*private, entao eh usaddo soh aqui dentro*/
    private static Graphics getGraphicsDaTela() {
        return tCenarioOndeSeDesenha.getGraphicsBuffer();
    }

}
