package Auxiliar;

import Fases.Fase;
import Modelo.Boost;
import Modelo.Elemento;
import Modelo.Hero;
import Modelo.Obstaculo;
import Modelo.Vilao;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ellia
 */
public class FaseXmlDomWriter {

    public static void saveToXml(Fase f, int numeroDaFase) {
        Document dom;
        Element e = null;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            // use factory to get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            Element root = dom.createElement("root");
            Element fase = dom.createElement("fase");
            fase.setAttribute("numeroDaFase", Integer.toString(numeroDaFase));
            fase.setAttribute("numeroDeBoosts", Integer.toString(f.getBoostsParaTerminar()));

            Element background = dom.createElement("background");
            Element caminhoSpriteBackground = dom.createElement("caminhoSprite");
            caminhoSpriteBackground.setTextContent(f.getCaminhoBackground());
            background.appendChild(caminhoSpriteBackground);
            fase.appendChild(background);

            Element heroi = createJogador(f.getHero(), dom);
            fase.appendChild(heroi);

            Element viloes = createViloes(dom);
            Element obstaculos = createObstaculos(dom);
            Element boosts = createBoosts(dom);

            for (int i = 0; i < f.getElementos().size(); i++) {
                Elemento elemento = f.getElementos().get(i);
                
                if (elemento.isbBoost()) {
                    createAndAddBoost(boosts, (Boost) elemento, dom);
                } else if (elemento.isVilao()) {
                    createAndAddVilao(viloes, (Vilao) elemento, dom);
                } else if (elemento.isObstaculo()) {
                    createAndAddObstaculo(obstaculos, (Obstaculo) elemento, dom);
                }
            }
            fase.appendChild(viloes);
            fase.appendChild(obstaculos);
            fase.appendChild(boosts);
            root.appendChild(fase);
            dom.appendChild(root);
            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//                tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
//                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                // send DOM to file
                tr.transform(new DOMSource(dom),
                        new StreamResult(new FileOutputStream("autosaves/unamed_save.xml")));

            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
        }
    }

    private static Element createJogador(Hero h, Document dom) {
        Element jogador = dom.createElement("jogador");
        setElementPosition(jogador, h);
        setElementBackground(jogador, h, dom);

        Element vidas = dom.createElement("vidas");
        vidas.setTextContent(Integer.toString(h.getNumVidas()));
        jogador.appendChild(vidas);
        return jogador;
    }

    private static Element createViloes(Document dom) {
        Element viloes = dom.createElement("viloes");
        return viloes;
    }

    private static Element createVilao(Vilao v, Document dom) {
        Element vilao = dom.createElement("vilao");
        setElementPosition(vilao, v);
        setElementBackground(vilao, v, dom);
        return vilao;
    }

    private static Element createAndAddVilao(Element viloes, Vilao v, Document dom) {
        Element vv = createVilao(v, dom);
        viloes.appendChild(vv);
        return viloes;
    }

    private static Element createBoost(Boost b, Document dom) {
        Element boost = dom.createElement("fruta");
        setElementPosition(boost, b);
        setElementBackground(boost, b, dom);
        return boost;
    }

    private static Element createAndAddBoost(Element boosts, Boost b, Document dom) {
        Element boostElementInDom = createBoost(b, dom);
        boosts.appendChild(boostElementInDom);
        return boosts;
    }

    private static Element createBoosts(Document dom) {
        Element viloes = dom.createElement("boost");
        return viloes;
    }

    private static Element createObstaculo(Obstaculo o, Document dom) {
        Element obs = dom.createElement("obstaculo");
        setElementPosition(obs, o);
        setElementBackground(obs, o, dom);

        Element id = dom.createElement("id");
        id.setTextContent(Integer.toString(o.getId()));
        obs.appendChild(id);
        return obs;
    }

    private static Element createAndAddObstaculo(Element obstaculos, Obstaculo o, Document dom) {
        Element obsElementInDom = createObstaculo(o, dom);
        obstaculos.appendChild(obsElementInDom);
        return obstaculos;
    }

    private static Element createObstaculos(Document dom) {
        Element obstaculos = dom.createElement("obstaculos");
        return obstaculos;
    }

    private static void setElementPosition(Element elementoDaDom, Elemento elementoDoJogo) {
        int linhaInicial = elementoDoJogo.getPosicao().getLinha();
        elementoDaDom.setAttribute("linhaInicial", Integer.toString(linhaInicial));
        int colunaInicial = elementoDoJogo.getPosicao().getColuna();
        elementoDaDom.setAttribute("colunaInicial", Integer.toString(colunaInicial));
    }

    private static void setElementBackground(Element elementoDaDom, Elemento elementoDoJogo, Document dom) {
        Element caminhoSprite = dom.createElement("caminhoSprite");
        caminhoSprite.setTextContent(elementoDoJogo.getCaminhoSprite());
        elementoDaDom.appendChild(caminhoSprite);
    }
}
