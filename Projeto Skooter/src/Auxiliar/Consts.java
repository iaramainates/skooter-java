package Auxiliar;

import java.io.File;

public class Consts {
    //Tamanho da Célula
    public static final int CELL_SIDE = 40;
    //Tamanho da Tela (RES células x RES células)
    public static final int RES = 11;
    //Intervalo de atualização de Frames
    public static final int FRAME_INTERVAL = 100;   
    //Tempo de Disparos em numero de frames (redesenhos)
    public static final int TIMER_DISPARO = 20;
    //Local das Imagens
    public static final String PATH = File.separator+"imgs"+File.separator;
    //Direcao da direita
    public static final int DIREITA = 1;
    //Direcao da esquerda
    public static final int ESQUERDA = 2;
    //Direcao de baixo
    public static final int BAIXO = 3;
    //Direcao de cima
    public static final int CIMA = 4;
}
